text = input("Please enter an upper-case string ending with a period: ")
upper = text.isupper()
period = text.endswith(".")
if upper and period:
        print("Text is OK") 
else:
        if not upper and period:
                print ("Text is not all upper case")
        elif upper and not period:
                print ("Text has no period at the end")
        elif not upper and not period:
                print ("Text is not all upper and has no period at the end")
